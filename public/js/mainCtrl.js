angular.module('flocApp').controller('mainCtrl', function($scope, $sce, $location){
 		
    $scope.trustSrc = function(map) {
        return $sce.trustAsResourceUrl(map);
    };
 		let restaurants = [
  {
    "apiKey": "90fd4587554469b1f15b4f2e73e76180992988ad497c8fa7",
    "logoUrl": "https://eatstreet-static.s3.amazonaws.com/assets/images/restaurant_logos/caffe-bianco-32626_1441038176730.png",
    "name": "Caffe Bianco",
    "streetAddress": "39 Sutter St",
    "distance": '0.5',
    "city": "San Francisco",
    "state": "CA",
    "zip": "94104",
    "phone": "(415) 421-2091",
    "latitude": 37.790098,
    "longitude": -122.401138,
    "url": "https://eatstreet.com/caffe-bianco/menu",
    "streetAddress":"250 Bush St",
    "city": "San Francisco",
    "state": "CA",
    "zip": "94109",
    "phone": "(415) 749-1938",
    "latitude": 37.788774,
    "longitude": -122.420697,
    "foodtypes": "pizza",
    "foods":"Hawaiian Pizza",
    "img":"http://www.pngall.com/wp-content/uploads/2016/05/Pizza-Free-PNG-Image.png",
    "price": 7.99,
    "url": "https://eatstreet.com/sammys-cafe/menu"
  },
  {
    "apiKey": "90fd4587554469b1f15b4f2e73e76180a13ed2152cfa024a",
    "logoUrl": "https://eatstreet-static.s3.amazonaws.com/assets/images/restaurant_logos/chicos-pizza-6th-st-30975_1432376174378.png",
    "name": "Chico's Pizza - 6th St.",
    "streetAddress": "131 6th St",
    "city": "San Francisco",
    "state": "CA",
    "zip": "94103",
    "phone": "(415) 255-1111",
    "distance":"1",
    "latitude": 37.780576,
    "longitude": -122.408187,
    "foodtypes": "pizza",
    "img":"http://www.pngall.com/wp-content/uploads/2016/05/Pizza-Free-PNG-Image.png",
    "foods":"Meat Lovers",
    "price": 7.99,
    "url": "https://eatstreet.com/chicos-pizza/menu"
  },
  {
    "apiKey": "90fd4587554469b1f15b4f2e73e7618085a92b1bbe04c6d2",
    "logoUrl": "https://eatstreet-static.s3.amazonaws.com/assets/images/restaurant_logos/chicos-pizza-ellis-st-30981_1432376192558.png",
    "name": "Chicos Pizza - Ellis St.",
    "streetAddress": "468 Ellis St",
    "city": "San Francisco",
    "state": "CA",
    "zip": "94102",
    "foodtypes": "pizza",
    "img":"http://www.pngall.com/wp-content/uploads/2016/05/Pizza-Free-PNG-Image.png",
    "foods":"Vegetarian Pizza",
    "price": 5.99,
    "phone": "(415) 776-2222",
    "distance":"1.4",
    "latitude": 37.784953,
    "longitude": -122.413918,
    "url": "https://eatstreet.com/chicos-pizza-ellis-st/menu"
  },
  {
    "apiKey": "90fd4587554469b1144247b91fbcb2f36c9afa0fd8fa00a8",
    "logoUrl": "https://eatstreet-static.s3.amazonaws.com/assets/images/restaurant_logos/pronto-pizza-20017_1407892811691.png",
    "name": "Pronto Burger",
    "streetAddress": "798 Eddy St.",
    "city": "San Francisco",
    "state": "CA",
    "zip": "94109",
    "foodtypes":"burger",
    "img":"http://www.drodd.com/images15/burger15.png",
    "foods":"Tasty Burger",
    "price": 8.99,
    "phone": "(415) 921-1999",
    "latitude": 37.782901,
    "longitude": -122.420725,
    "url": "https://eatstreet.com/pronto-pizza-san-francisco/menu"
  },
  {
    "apiKey": "90fd4587554469b1f15b4f2e73e76180cb50207918af22bd",
    "logoUrl": "https://eatstreet-static.s3.amazonaws.com/assets/images/restaurant_logos/dijon-burger-36303_1459971434457.png",
    "name": "Dijon Burger",
    "streetAddress": "1428 Polk St.",
    "city": "San Francisco",
    "state": "CA",
    "zip": "94109",
    "img":"http://www.drodd.com/images15/burger15.png",
    "foodtypes":"burger",
    "foods":"Fireball Burger",
    "price": 14.50,
    "phone": "(415) 614-1100",
    "url": "https://eatstreet.com/dijon-burger/menu"
  },
  {
    "apiKey": "90fd4587554469b1884225aec137a02acc52b5c7cfb771df",
    "logoUrl": "https://eatstreet-static.s3.amazonaws.com/assets/images/restaurant_logos/frjtz-18415_1406637889162.png",
    "name": "Frjtz",
    "streetAddress": "590 Valencia St",
    "city": "San Francisco",
    "state": "CA",
    "zip": "94110",
    "foodtypes":"burger",
    "foods":"Vegan Burger",
    "img":"http://www.drodd.com/images15/burger15.png",
    "price": 7.99,
    "phone": "(415) 863-8272",
    "latitude": 37.763508,
    "longitude": -122.422097,
    "url": "https://eatstreet.com/fritz/menu"
  },
  {
    "apiKey": "90fd4587554469b1f15b4f2e73e76180992988ad497c8fa7",
    "logoUrl": "https://eatstreet-static.s3.amazonaws.com/assets/images/restaurant_logos/caffe-bianco-32626_1441038176730.png",
    "name": "Caffe Bianco",
    "streetAddress": "39 Sutter St",
    "city": "San Francisco",
    "state": "CA",
    "zip": "94104",
    "phone": "(415) 421-2091",
    "latitude": 37.790098,
    "longitude": -122.401138,
    "url": "https://eatstreet.com/caffe-bianco/menu",
    "streetAddress":"250 Bush St",
    "city": "San Francisco",
    "state": "CA",
    "zip": "94109",
    "phone": "(415) 749-1938",
    "latitude": 37.788774,
    "longitude": -122.420697,
    "foodtypes": "burrito",
    "img":"http://foodnetwork.sndimg.com/content/dam/images/food/fullset/2013/2/14/0/FNK_breakfast-burrito_s4x3.jpg.rend.sniipadlarge.jpeg",
    "foods":"El Burrito",
    "price": 7.99,
    "url": "https://eatstreet.com/sammys-cafe/menu"
  },
  {
    "apiKey": "90fd4587554469b1884225aec137a02acc52b5c7cfb771df",
    "logoUrl": "https://eatstreet-static.s3.amazonaws.com/assets/images/restaurant_logos/frjtz-18415_1406637889162.png",
    "name": "Frjtz",
    "streetAddress": "590 Valencia St",
    "city": "San Francisco",
    "state": "CA",
    "zip": "94110",
    "foodtypes":"burrito",
    "foods":"Super Burrito",
    "img":"http://foodnetwork.sndimg.com/content/dam/images/food/fullset/2013/2/14/0/FNK_breakfast-burrito_s4x3.jpg.rend.sniipadlarge.jpeg",
    "price": 8.99,
    "phone": "(415) 863-8272",
    "latitude": 37.763508,
    "longitude": -122.422097,
    "url": "https://eatstreet.com/fritz/menu"
  }
]
 $scope.confirmation = false;
    $scope.getData = function(term){
      console.log($location.$$absUrl);
      $scope.show = true;
      $scope.ordered = false;
     
      $scope.canceled = false;
      $scope.method = "checked";
    	console.log(term);
    	var food = restaurants.filter(function(restaurants){
				return restaurants.foodtypes === term;
			});
      for(var i = 0; i < food.length; i++){
        food[i].search = term;
      }
			$scope.foods = food;
    	console.log($scope.foods);
    };

    function getParameterByName(name, url) {
        if (!url) {
          url = window.location.href;
        }
        name = name.replace(/[\[\]]/g, "\\$&");
        var regex = new RegExp("[?&]" + name + "(=([^&#]*)|&|#|$)"),
            results = regex.exec(url);
        if (!results) return null;
        if (!results[2]) return '';
        return decodeURIComponent(results[2].replace(/\+/g, " "));
    }

    var query = JSON.parse(unescape(getParameterByName('flockEvent'))).text.toLowerCase();
    var food = '';
    if (query != '') {
        if (query.indexOf('pizza') > -1) {
            food = 'pizza';
        }
        if (query.indexOf('burrito') > -1) {
            food = 'burrito';
        }
        if (query.indexOf('burger') > -1) {
            food = 'burger';
        }
        if (query.indexOf('here') > -1) {
            // default to delivery
            setTimeout(function () {
                $('input[type=radio][value=Delivery]').trigger('click');
            }, 100)
        }
    }


    $scope.getData(food);

    if (query.indexOf('sudo') > -1) {
        // auto click the first item
        setTimeout(function () {
            $($('.dish')[0]).trigger('click')
        }, 100);
    }


    $scope.confirmation = function(name, method){
    	console.log(method);
    	console.log(name);
    	var choice = restaurants.filter(function(restaurants){
    		return restaurants.foods === name;
    	});
    	choice[0].method = method;
      $scope.show = false;
      $scope.confirmation = false;
    	if(method === 'Pick up'){
        $scope.map = 'https://www.google.com/maps/embed/v1/place?key=AIzaSyBG1FaMjyrJZWHUprmlMrpZBIwq_TH-hNs&q=' + choice[0].name;
    	}
    	$scope.selection = choice;
    	console.log($scope.selection);
    }

    $scope.delete = function(){
      $scope.canceled = true;
       $scope.confirmation = true;
    }

    $scope.placed = function(){
      $scope.ordered = true;
       $scope.confirmation = true;
    }

});
