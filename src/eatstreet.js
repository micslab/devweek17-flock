'use strict';
const express = require('express');
const app = express();
const request = require('request-promise');

const token = '1170c0df4e16c855';
let method;
let distance;
let fullAddress;
let search;

//Test Data:
// const token = '1170c0df4e16c855';
// let method = 'delivery';
// let distance = '10';
// let fullAddress = '44 Tehama St, San Francisco, CA, 94105';
// let search = 'burrito';


// let param = "https://api.eatstreet.com/publicapi/v1/restaurant/search-test?method=" + method + "&pickup-radius=" + distance +"&search=" + search + "&street-address=" + fullAddress;

const options = {
	method: 'GET',
	uri: 'https://api.eatstreet.com/publicapi/v1/restaurant/search',
	header:{
		'User-Agent':'Request-Promise',
		'X-Access-Token': token
	},
	qs:{
		access_token: token,
		method: method,
		pickup_radius: distance,
		search: search,
		street_address: fullAddress
	},
	json: true
};



app.get('/search', (req, res) =>{
	request(options)
		.then((data) => {
			for(var i = 0; i < 4; i++){
				food.push(data.restaurants[i]);
			}
		})
		.catch((err) => {
			console.log(err);
		});
});


app.listen(8000);

// access_token: token,
// curl -X GET \ 
//  -H 'X-Access-Token: ac23fed27672424c' \
//  'https://api.eatstreet.com/publicapi/v1/restaurant/search-test?method=pickup&pickup-radius=25&search=sushi&street-address=111+pine+st+san+francisco+ca+94111'