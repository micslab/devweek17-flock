'use strict';

let flock = require('flockos');
let request = require('request');
 
flock.appId = '866892d4-7fa8-4545-a16d-1606586c820c';
flock.appSecret = 'cae2b30e-8a41-438e-be84-649ed1e47af4';

let express = require('express');
let app = express();
let storage = require('node-persist');
let rando = require('random-number-in-range');
let colors = require('colors');

let token = '';
let comments = [
	'Yum!',
	'Awesome, I love',
	'FLOCK YEAH!',
	'Delicious',
	'mmmmmm',
	'me so hangry! want',
	'cooking up a'
];

app.use(express.static('public'));
app.use(flock.events.tokenVerifier);
app.post('/events', flock.events.listener);

storage.connected = false;

storage.init( /* options ... */ ).then(function() {
	storage.connected = true;
	try {
		storage.getItem('token').then(function(value) {
			console.log({
				arguments: arguments,
				value: value
			});
			token = value;
			console.log('flockoli'.bold.green, '\trunning with token', '\t', token);
		});
	} catch (e) {
		console.log('ERROR'.bold.red, '\tcannot find a stored token');
	}
});

flock.events.on('app.install', function (event, callback) {
	// console.log("install", arguments)
	console.log('APP INSTALL'.yellow.bold);
	
	token = event.token;
	if (storage.connected) {
		storage.setItem('token', token);
	};
	callback();
});

flock.events.on('client.slashCommand', function (event, callback) {
	// console.log('received slashCommand', arguments);

	flock.callMethod('users.getInfo', token, {
	    id: event.userId,
	    token: token,
	}, function (error, data) {
	    // console.log('\n\nfull user details', arguments);
	    if (!error) {
	    	let response = [
	    		comments[rando(0, comments.length)],
	    		' ',
	    		event.text.toUpperCase(),
	    		' for ',
	    		data.email
	    	].join('');

	    	callback(null, { text: response });
	    }
	});
});

app.listen(8000);

/*
flock.callMethod('chat.sendMessage', token, {
    to: 'u:wufu4udrcewerudu',
    text: 'hello'
}, function (error, response) {
    if (!error) {
        console.log(response);
    }
});*/